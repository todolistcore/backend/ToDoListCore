'use strict'

//Components
let express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
let mongoDB = require("./Database/MongoConnection");
let ApiRoutes = require("./Routes/Api");

//Instances
var app = express();

//middleware
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(cors());
app.use('/',ApiRoutes); //Load Routes
 

module.exports = app;