'use strict'
let api = require("./app");
const port = process.env.PORT || 3000;


//Listen Server
api.listen(port,()=>{
    console.log(`Server is Running on Port: ${port}`);
});